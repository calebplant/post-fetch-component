# Fetch Comments Component

## Overview

A lightning web component that fetches posts/associated comments from two endpoints and displays them in a paginated list with collapsable comments. When building the component I went with a [SLDS Feed](https://lightningdesignsystem.com/components/feeds/#site-main-content) to allow for easy future expansion if the endpoints return additional data.

## Demo (~40 sec)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=biNNSEXEVss)

## Screenshot

![Displayed Posts](media/screenshot.png)

