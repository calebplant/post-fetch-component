public with sharing class PostFeedController {

    private static final String POSTS_ENDPOINT = 'https://jsonplaceholder.typicode.com/posts';
    private static final String COMMENTS_ENDPOINT = 'https://jsonplaceholder.typicode.com/comments';
    
    @AuraEnabled(cacheable=true)
    // public FeedResponse getPostsAndComments()
    public static FeedResponse getPostsAndComments()
    {
        // Instantiate a new http object
        Http h = new Http();
        // Instantiate a new HTTP request
        HttpRequest req = new HttpRequest();
        req.setEndpoint(POSTS_ENDPOINT);
        req.setMethod('GET');


        // Send the request, and return a response
        HttpResponse res = h.send(req);
        // System.debug('body:');
        // System.debug(res.getBody());
        List<PostJson> postList = (List<PostJson>)Json.deserialize(res.getBody(), List<PostJson>.Class);
        // for(PostJson eachPost : postList) {
        //     System.debug(eachPost);
        // }
        req.setEndpoint(COMMENTS_ENDPOINT);
        res = h.send(req);
        List<CommentJson> commentList = (List<CommentJson>)Json.deserialize(res.getBody(), List<CommentJson>.Class);

        FeedResponse reponse = new FeedResponse(postList, commentList);
        return reponse;
    }

    public class FeedResponse
    {
        @AuraEnabled
        public List<PostResponse> posts = new List<Postresponse>();

        public FeedResponse(List<PostJson> fetchedPosts, List<CommentJson> fetchedComments)
        {
            System.debug('START FeedResponse');
            Map<String, List<CommentJson>> commentsByPostId = new Map<String, List<CommentJson>>();
            // Map each post's comments by post Id
            for(CommentJson eachComment : fetchedComments) {
                if(commentsByPostId.containsKey(eachComment.postId)) {
                    List<CommentJson> comments = commentsByPostId.get(eachComment.postId);
                    comments.add(eachComment);
                    commentsByPostId.put(eachComment.postId, comments);
                }
                else {
                    commentsByPostId.put(eachComment.postId, new List<CommentJson> { eachComment } );
                }
            }
            for(PostJson eachPost : fetchedPosts) {
                posts.add(new PostResponse(eachPost, commentsByPostId.get(eachPost.id)));
            }
        }
    }

    public class PostResponse
    {
        @AuraEnabled
        public String userId;
        @AuraEnabled
        public String id;
        @AuraEnabled
        public String title;
        @AuraEnabled
        public String body;
        @AuraEnabled
        public List<CommentResponse> comments = new List<CommentResponse>();

        public PostResponse(PostJson passedPost, List<CommentJson> passedComments)
        {
            System.debug('START PostResponse');
            userId = passedPost.userId;
            id = passedPost.id;
            title = passedPost.title;
            body = passedPost.body;
            for(CommentJson eachComment : passedComments) {
                comments.add(new CommentResponse(eachComment));
            }
        }
    }

    public class CommentResponse
    {
        @AuraEnabled
        public String postId;
        @AuraEnabled
        public String id;
        @AuraEnabled
        public String name;
        @AuraEnabled
        public String email;
        @AuraEnabled
        public String body;

        public CommentResponse(CommentJson passedComment)
        {
            System.debug('START CommentResponse');
            id = passedComment.id;
            name = passedComment.name;
            email = passedComment.email;
            body = passedComment.body;
        }
    }

    public class PostJson {
        public String userId;
        public String id;
        public String title;
        public String body;
    }

    public class CommentJson {
        public String postId;
        public String id;
        public String name;
        public String email;
        public String body;
    }
}
