import { api, LightningElement } from 'lwc';

export default class PostItem extends LightningElement {

    @api postData;
    showComments = false;

    get commentsIcon() {
        return this.showComments ? "utility:dash" : "utility:add";
    }

    // Handlers
    handleShowComments() {
        console.log('postItem :: handleShowComments');
        this.showComments = !this.showComments;
    }
}