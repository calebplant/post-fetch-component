import { LightningElement, wire } from 'lwc';
import getPosts from '@salesforce/apex/PostFeedController.getPostsAndComments';

export default class PostFeed extends LightningElement {

    postData;

    resultPerPage=10;
    totalPages;
    currentPage;
    
    @wire(getPosts)
    fetchPosts(response) {
        if(response.data) {
            console.log('Response from server: ');
            console.log(response.data);
            this.postData = response.data;
            this.updatePagination();
        }
        if(response.error) {
            console.log('Error fetching posts!');
            console.log(response.error);
        }
    }

    get displayedPosts() {
        if(this.postData) {
            let start = Number(this.currentPage) * Number(this.resultPerPage);
            let end = Number(start) + Number(this.resultPerPage);
            return this.postData.posts.slice(start, end);
        }
    }

    get currentPageLabel() {
        return this.currentPage.toString();
    }

    get nextDisabled() {
        return (this.currentPage == (this.totalPages - 1) || this.currentPage == undefined);
    }

    get previousDisabled() {
        return (this.currentPage === 0 || this.currentPage == undefined);
    }

    get pageLabel() {
        if(this.postData && this.resultPerPage) {
            let start = Number(this.currentPage) * Number(this.resultPerPage) + 1;
            let end = Number(start) + Number(this.resultPerPage) - 1;
            end = end > this.postData.posts.length ? this.postData.posts.length : end;

            return 'Displaying ' + start + ' - ' + end + ' of ' + this.postData.posts.length;
        }
        return '';
    }

    get pageOptions() {
        let options = [];
        for(let i=0; i < this.totalPages; i++) {
            options.push({label: i+1, value: i.toString()});
        }
        return options;
    }

    // Handlers
    handleSectionToggle(event) {
        console.log('postFeed :: handleSectionToggle');
    }

    handlePageChange(event) {
        console.log('postFeed :: handlePageChange');
        switch(event.currentTarget.name){
            case 'previous':
                this.currentPage--;
                break;
            case 'next':
                this.currentPage++;
                break;
        }
    }

    handlePageSelect(event) {
        console.log('postFeed :: handlePageSelect');
        this.currentPage = parseInt(event.detail.value);
    }

    // Utils
    updatePagination() {
        this.currentPage = 0;
        this.totalPages = Math.ceil(this.postData.posts.length / this.resultPerPage);
        console.log('totalPages:');
        console.log(this.totalPages);
    }
}