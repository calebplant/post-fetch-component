import { api, LightningElement } from 'lwc';

export default class CommentItem extends LightningElement {

    @api commentData;
}